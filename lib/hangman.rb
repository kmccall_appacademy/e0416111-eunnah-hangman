class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players = {})
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  def play
    setup
    take_turn until over?
    conclude
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  # ask guesser for guess, referee checks it, then update board
  def take_turn
    guess = @guesser.guess(board)
    indices = @referee.check_guess(guess)
    update_board(indices, guess)
    @guesser.handle_response(guess, indices)
  end

  private

  def update_board(indices, letter)
    if indices != []
      indices.each { |idx| @board[idx] = letter }
    end
  end

  def over?
    @board.count(nil) == 0
  end

  def conclude
    @referee.print_board(@board)
    puts "Congrats on guessing the word!"
  end

end


# guess letters and confirm guesses
class HumanPlayer

  attr_accessor :dictionary, :word, :length

  def initialize
  end

  # returns the length of a word in the dictionary
  def pick_secret_word
    print "Referee, provide the length of your secret word: "
    @length = gets.chomp.to_i
  end

  def register_secret_length(secret_word_length)
    puts "Length of word: #{secret_word_length} characters"
  end

  def guess(board)
    print_board(board)
    puts "Guess a character"
    gets.chomp
  end

  # returns the indices of the found letters and handles incorrect guesses
  def check_guess(guess)
    puts "Please tell the computer what indices (if any) were correct, e.g. '0, 3'"
    puts "If no indices were correct, simply type 'n'"
    indices = gets.chomp
    if indices == "n"
      []
    else
      indices = indices.split(", ")
      indices.map { |el| el.to_i }
    end
  end

  def handle_response
  end

  # print out the board
  def print_board(board)
    board_string = board.map do |el|
      el.nil? ? "_" : el
    end
    puts "Secret word: #{ board_string.join }"
  end

end

# guess letters and confirm guesses
class ComputerPlayer

  attr_accessor :dictionary, :word

  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = dictionary
  end

  def self.read_dictionary
    File.readlines("lib/dictionary.txt").map(&:chomp)
  end

  def pick_secret_word
    @word = @dictionary[rand(0...dictionary.length)]
    @word.length
  end

  # rejects words of the wrong length from #candidate_words
  def register_secret_length(secret_word_length)
    @secret_word_length = secret_word_length
    @candidate_words.each do |word|
      if word.length != @secret_word_length
        @candidate_words.delete(word)
      end
    end
  end

  def guess(board)
    print_board(board)

    letter_counts = Hash.new(0)
    ("a".."z").to_a.each do |letter|
      if !board.include?(letter)
        letter_counts[letter] = @candidate_words.join.count(letter)
      end
    end

    guess = letter_counts.sort_by { |key, value| value }.last[0]

    puts "Computer guesses " + guess
    guess
  end

  # return indices of found letters. also, handle an incorrect guess
  def check_guess(guess)
    indices = []
    @word.each_char.with_index do |char, idx|
      indices << idx if char == guess
    end

    if indices == []
      puts "Sorry, no matches!"
      []
    else
      indices
    end
  end

  # rejects non-matching words from #candidate_words, and also rejects words
  # containing the guessed letter
  def handle_response(guess, indices)
    # reject words containing the guessed letter

    if indices == []
      @candidate_words.each do |word|
        @candidate_words.delete(word) if word.include?(guess)
      end
    else
      # reject non-matching words
      @candidate_words.each do |word|
        indices.each do |idx|
          if word[idx] != guess || word.count(guess) != indices.length
            @candidate_words.delete(word)
          end
        end
      end
    end
  end

  # print out the board to the human player
  def print_board(board)
    board_string = board.map do |el|
      el.nil? ? "_" : el
    end
    puts "Secret word: #{ board_string.join }"
  end

  # returns list of words not yet eliminated for consideration as a guess
  def candidate_words
    @candidate_words
  end

end

if __FILE__ == $PROGRAM_NAME
  dictionary = ComputerPlayer.read_dictionary
  human_player = HumanPlayer.new
  computer_player = ComputerPlayer.new(dictionary)

  players = { guesser: computer_player, referee: human_player }
  game = Hangman.new(players)
  game.play
end
